// these constants describe the pins. They won't change:
const int groundpin = 18;             // analog input pin 4 -- ground
const int powerpin = 19;              // analog input pin 5 -- voltage
const int ypin = A2;                  // x-axis of the accelerometer
const int xpin = A1;                  // y-axis
const int zpin = A3;                  // z-axis (only on 3-axis models)

void setup() {
  // initialize the serial communications:
  Serial.begin(9600);

  // Provide ground and power by using the analog inputs as normal digital pins.
  // This makes it possible to directly connect the breakout board to the
  // Arduino. If you use the normal 5V and GND pins on the Arduino,
  // you can remove these lines.
  pinMode(groundpin, OUTPUT);
  pinMode(powerpin, OUTPUT);
  digitalWrite(groundpin, LOW);
  digitalWrite(powerpin, HIGH);
}

void loop() {
  // print the sensor values:
  Serial.print(analogRead(xpin)-1023);
  // print a tab between values:
  Serial.print("\n");
  Serial.print(analogRead(ypin)-1023);
 //  print a tab between values:
  Serial.print("\n");
Serial.print(analogRead(zpin)-500);
  Serial.println();
  // delay before next reading:
  delay(100);
  
}
