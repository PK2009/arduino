#include <WiFiEsp.h>
#include <WiFiEspClient.h>
#include <WiFiEspUdp.h>
#include <SoftwareSerial.h>
#include <PubSubClient.h>
#include <SPI.h>
IPAddress ip(192,168,8,1);
char ssid[] = "A"; // your network SSID (name)
char pass[] = "@shok510"; // your network password
int status = WL_IDLE_STATUS; // the Wifi radio's status
int photocellPin = 4; // Analog input pin on Arduino we connected the SIG pin from sensor
int photocellReading; // Here we will place our reading
char server[] =  "192.168.0.9";
// Initialize the Ethernet client object
WiFiEspClient espclient;
SoftwareSerial soft(0,1); // RX, TX

void setup() {
  // initialize serial for debugging
  //Serial.begin(9600);
  // initialize serial for ESP module
  soft.begin(115200);
  // initialize ESP module
  // attempt to connect to WiFi network
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  // you're connected now, so print out the data
  Serial.println("You're connected to the network");
}

void loop() {
  photocellReading = analogRead(photocellPin);
  // Fill the sensorReading with the information from sensor
  // Connect to the server (your computer or web page)
  if (espclient.connect(server, 80)) {
    espclient.println("GET /write_data7.php?"); // This
    espclient.println("value="); // This
    espclient.println(photocellReading);
    // And this is what we did in the testing section above.
    // We are making a GET request just like we would from our browser
    // but now with live data from the sensor
    espclient.println(" HTTP/1.1"); // Part of the GET request
    espclient.println("Host: 192.168.8.1");
    // IMPORTANT: If you are using XAMPP you will have
    //to find out the IP address of your computer and put it here
    // (it is explained in previous article). If you have a web page,
    // enter its address (ie.Host: "www.yourwebpage.com")
    espclient.println("Connection: close");
    // Part of the GET request telling the server
    // that we are over transmitting the message
    espclient.println(); // Empty line
    espclient.println(); // Empty line
    espclient.stop();    // Closing connection to server
  } else {
    // If Arduino can't connect to the server (your computer or web page)
    Serial.println("--> connection failed\n");
  }

  // Give the server some time to recieve the data and store it. I used 10 seconds here. Be advised when delaying. If u use a short delay, the server might not capture data because of Arduino transmitting new data too soon.
  delay(10000);
}
