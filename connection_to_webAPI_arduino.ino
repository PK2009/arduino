#include <ESP8266WiFi.h>
const char* ssid     = "D-link";
const char* password = "@shok510";
String pubString;
const char* host = "api.devicehub.net";//api.devicehub.net
//int pH_pin = A0; // Set  pin you are using to read data. In this case it's analog for reading pH values
//float pH = 0; // Initialize the value you are going to read
//const int sleepTimeS = 1*10; // Set sleep time in seconds for the deep sleep cycle


byte statusLed    = 13;

byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;

volatile byte pulseCount;  

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;

void setup() {
  // Initialize a serial connection for reporting values to the host
  Serial.begin(115200);
   
  // Set up the status LED line as an output
  pinMode(statusLed, OUTPUT);
  digitalWrite(statusLed, HIGH);  // We have an active-low LED attached
  
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

  // The Hall-effect sensor is connected to pin 2 which uses interrupt 0.
  // Configured to trigger on a FALLING state change (transition from HIGH
  // state to LOW state)
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  delay(100);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP()); // This is your NodeMCU IP address. Could be handy for other projects

  POST(); // Post the value to the web database
  //GET();  // If all you need to do is push data, you don't need this

  //Serial.println("ESP8266 in sleep mode");
  // Put NodeMCU in deep sleep. When it wakes up it will run setup() again,
  // connect to WiFi, then post and/or get data, then go back to sleep and repeat
  //ESP.deepSleep(sleepTimeS * 1000000);
}

void loop()
{
  // This is empty because in order for the sleep cycle to work all code is in setup()
  if((millis() - oldTime) > 1000)    // Only process counters once per second
  { 
    // Disable the interrupt while calculating flow rate and sending the value to
    // the host
    detachInterrupt(sensorInterrupt);
        
    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (litres/minute in
    // this case) coming from the sensor.
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    
    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    oldTime = millis();
    
    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (flowRate / 60) * 1000;
    
    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;
      
    unsigned int frac;
    
    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    Serial.print(flowMilliLitres);
    Serial.print("mL/Sec");

    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    Serial.print(totalMilliLitres);
    Serial.println("mL"); 

    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }
}

void POST(void)
{
  delay(5000);  // You can get rid of this or decrease it
  //pH = analogRead(pH_pin); // Read pH value

  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  pubString = "{\"value\": " + String(totalMilliLitres) + "}";
  String pubStringLength = String(pubString.length(), DEC);
  // We now create a URI for the request
  Serial.print("Requesting POST: ");
  // Send request to the server:
  client.println("POST 192.168.0.105/smartmeter/test.php HTTP/1.1");
  // Note: replace the ALL CAPS parts above with info from DeviceHub.net. After setting up an account,
  // add a project, a device (NodeMCU), a sensor (like pH_Sensor), and actuator if you need. After you set
  // up the project it should show you an API key, device UUID, and of course the sensor name you entered.
  //client.println("Host: api.devicehub.net");
  //client.print("X-ApiKey: YOUR_API_KEY\r\n"); // Get this from DeviceHub.net
  client.println("Content-Type: application/json");
  client.println("Connection: close");
  client.print("Content-Length: ");
  client.println(pubStringLength);
  client.println();
  client.print(pubString);
  client.println();
  delay(500); // Can be changed
  
  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  Serial.println();
  Serial.println("closing connection");
}

// If you only need to post data to the database then you don't need the code below:
/*void GET(void)
{
  int state_pos;
  String state;
  delay(5000);
  pH = analogRead(pH_pin); // Read pH value
  Serial.print("connecting to ");
  Serial.println(host);
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  pubString = "{\"value\": " + String(pH) + "}";
  String pubStringLength = String(pubString.length(), DEC);
  // We now create a URI for the request
  Serial.print("Requesting GET: ");
  // This will send the request to the server
  client.println("GET /v2/project/PROJECT_ID/device/Device_UUID/actuator/ACTUATOR_NAME/state HTTP/1.1");
  client.println("Host: api.devicehub.net");
  client.print("X-ApiKey: YOURAPIKEY\r\n");
  client.println("Connection: close");
  client.println();
  delay(500);
  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    state_pos = line.indexOf("state");
    if (state_pos > 0)
      state = line.substring(state_pos + 7, state_pos + 8);
    else
      state = "nothing";
    //Serial.println(state);
    if (state == "1")
      Serial.println("LED ON");
      digitalWrite(BUILTIN_LED, LOW); // You can change this
    if (state == "0")
      Serial.println("LED OFF");
      digitalWrite(BUILTIN_LED, HIGH);
    //Serial.print(line);
  }
  Serial.println();
  Serial.println("closing connection");
}*/

void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
